package fragment

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import basica.contato.Contato
import com.ozeppelin.kotlincesar.R
import kotlinx.android.synthetic.main.fragment_cadastro_contato.*
import kotlinx.android.synthetic.main.fragment_cadastro_contato.view.*



class ContatoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cadastro_contato, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.buttonSalvar.setOnClickListener(  {view ->forResultTelaListActivity(view)})
        view.buttonCancelar.setOnClickListener(  {view ->forResultTelaListActivity(view)})
    }




    fun forResultTelaListActivity( view: View){
       var   contato = Contato(editNome.text?.toString(),editEmail.text?.toString(),editNumeroTelefone.text?.toString(),0,"")

        if(view== buttonSalvar) {
            if (validarCampos(contato)) {
                val intent = Intent()
                intent.putExtra("contato", contato)
                activity?.setResult(RESULT_OK, intent)
                activity?.finish()
            }
        }else{
            activity?.setResult(RESULT_CANCELED)
            activity?.finish()
        }

    }

     fun validarCampos(contato: Contato): Boolean{

        if(contato?.nome?.trim().equals("")){
            Toast.makeText(activity,R.string.nomeObrigatorio,Toast.LENGTH_SHORT).show()
            return false
        }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(contato?.email).matches()) {
            Toast.makeText(activity,R.string.formatoEmail,Toast.LENGTH_SHORT).show()
            return false
        }

        if(!android.util.Patterns.PHONE.matcher(contato?.telefone).matches()) {
            Toast.makeText(activity, R.string.formatoTelefone, Toast.LENGTH_SHORT).show()
            return false
        }

        return true


    }

}
