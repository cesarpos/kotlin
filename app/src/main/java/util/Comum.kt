package util

import android.content.Context
import android.net.ConnectivityManager

object Comum {

    fun verificarConexaoInternet (context: Context): Boolean {
        val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mNetWork = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (mWifi.isConnected || mNetWork.isConnected) {
            return true
        }
        return false
    }
}