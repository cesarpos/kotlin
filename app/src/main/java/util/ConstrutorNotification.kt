package util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat

object  ConstrutorNotification {


     fun criarNotificacaoComPendingIntent (context: Context,requestCodePendingIntent: Int, intent: Intent,
                                           flagIntent: Int, channelId: String,
                                           smallIcon: Int, contentTitulo: String,contentText: String, idNotificationManager: Int){


         val pendingIntent =  PendingIntent.getActivity(context,requestCodePendingIntent,intent, flagIntent)
         val notificationChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
             NotificationChannel(channelId,"0", NotificationManager.IMPORTANCE_HIGH)
         } else {
             TODO("VERSION.SDK_INT < O")
         }

         val builderNotification = NotificationCompat.Builder(context!!,channelId)
                 .setContentTitle(contentTitulo)
                 .setContentText(contentText)
                 .setContentIntent(pendingIntent)
                 .setSmallIcon(smallIcon).build()

         val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
             notificationManager.createNotificationChannel(notificationChannel)
         }
         notificationManager.notify(idNotificationManager,builderNotification)


     }


    fun criarNotificacaoSemPendingIntent(context: Context, channelId: String,
                                         smallIcon: Int, contentTitulo: String,contentText: String, idNotificationManager: Int){

        val notificationChannel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(channelId,"0", NotificationManager.IMPORTANCE_HIGH)
        } else {
            TODO("VERSION.SDK_INT < O")
        }

        val builderNotification = NotificationCompat.Builder(context!!,channelId)
                .setContentTitle(contentTitulo)
                .setContentText(contentText)
                .setSmallIcon(smallIcon).build()

        val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(notificationChannel)
        }
        notificationManager.notify(idNotificationManager,builderNotification)
    }


}