package Volley

import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import java.lang.reflect.Method
import java.util.HashMap

class ServiceVolley: ServiceInterface {

    val TAG = ServiceVolley::class.java.simpleName
    val basePath = "https://api.punkapi.com/v2/beers/1"

    override fun get(completionHandler: (response: JSONObject?) -> Unit) {

        val jsonObjReq = object : JsonObjectRequest(Request.Method.GET, basePath, null, Response.Listener { response ->
            Log.d(TAG, "/post request OK! Response: $response")
            completionHandler(response)
        }, Response.ErrorListener { error ->
            VolleyLog.e(TAG, "/get request fail! Error: ${error.message}")
            completionHandler(null)
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }
        BackendVolley.instance?.addToRequestQueue(jsonObjReq, TAG)

    }

}