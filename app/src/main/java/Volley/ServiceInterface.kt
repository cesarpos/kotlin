package Volley

import org.json.JSONObject
import java.nio.channels.CompletionHandler

interface ServiceInterface {

    fun get(completionHandler: (response: JSONObject?) -> Unit)

}