package Volley

import org.json.JSONObject

class APIController constructor(serviceInjection: ServiceInterface): ServiceInterface {

    private val service: ServiceInterface = serviceInjection

    override fun get(completionHandler: (response: JSONObject?) -> Unit) {
        service.get(completionHandler)
    }

}