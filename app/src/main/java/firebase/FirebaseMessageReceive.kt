package firebase

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.ozeppelin.kotlincesar.R
import util.ConstrutorNotification

class FirebaseMessageReceive : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)

        Log.i("fdsf", "From: " + p0?.getFrom());
        Log.i("dataFirebase", "From: " + p0?.getData());
        ConstrutorNotification.criarNotificacaoSemPendingIntent(this,"FIREBASE", R.mipmap.ic_launcher,"Firebase Title",
                p0?.notification?.body.toString(),0);

    }
}