package cadastroContato

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import com.ozeppelin.kotlincesar.R
import fragment.ContatoFragment

class CadastroContatoActivity  : FragmentActivity() {

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_cadastro);

        val contatoFragment = ContatoFragment()
        val transaction = getSupportFragmentManager().beginTransaction()
        transaction.add(R.id.mainFragment,contatoFragment)
        transaction.commit()
    }


}