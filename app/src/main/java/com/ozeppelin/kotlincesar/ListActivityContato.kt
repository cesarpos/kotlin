package com.ozeppelin.kotlincesar

import Volley.APIController
import Volley.ServiceVolley
import adapter.AdapterContato
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import basica.contato.Contato
import broadcast.BroadcastService
import cadastroContato.CadastroContatoActivity
import kotlinx.android.synthetic.main.tela_lista_contato.*
import service.ServiceContato
import util.Comum
import util.ConstrutorNotification
import org.json.JSONObject



class ListActivityContato : AppCompatActivity() {
    private var REQUEST_CODE_CADASTRO_ACTIVITY: Int = 1
    private var contatoList: ArrayList<Contato>?   = ArrayList()
    private var posicaoListaConatato: Int = 0
    private var contatoCadastrado : Contato? = null
    var serviceq : ServiceContato? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tela_lista_contato)

        val recyclerViewContato = recyclerViewContato
        recyclerViewContato.adapter =  AdapterContato(Contato.contatoList)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerViewContato.layoutManager = layoutManager
        //BroadCast registrando
        val intentFilter = IntentFilter("toastBroadCast");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadCastReceiver(),intentFilter)


        //Criando Alarm para executar serviço a cada 10 segundos
        val  intentService = Intent(this, BroadcastService::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this,0,intentService,PendingIntent.FLAG_ONE_SHOT)
        val alarmManager =  getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),10000,pendingIntent)

        //Criando o get request
        val serviceVolley = ServiceVolley()
        val apiController = APIController(serviceVolley)

        apiController.get { response ->
            print(response.toString())
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver())
    }


    fun cadastrarNovoContato(view: View){
        val intent = Intent(this, CadastroContatoActivity::class.java)
        startActivityForResult(intent,REQUEST_CODE_CADASTRO_ACTIVITY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(REQUEST_CODE_CADASTRO_ACTIVITY==1){
            if(resultCode== Activity.RESULT_OK){
                contatoCadastrado =  data?.getParcelableExtra<Contato>("contato")
                contatoCadastrado?.id = Contato.contatoList?.size?:+1
                Contato.contatoList?.add(contatoCadastrado!!)
                posicaoListaConatato = Contato.contatoList?.size?.minus(1)!!
                recyclerViewContato.adapter.notifyDataSetChanged()

                //sendBroadcast(Intent("com.ozeppelin.kotlincesar"))
                if(Comum.verificarConexaoInternet(this)) {
                    LocalBroadcastManager.getInstance(this).sendBroadcast(Intent("toastBroadCast"))
                }


            } else {
                Toast.makeText(this,R.string.cancelar_Cadastro,Toast.LENGTH_SHORT).show()
            }

        }
    }



     fun broadCastReceiver (): BroadcastReceiver{

         val broadCastReceiver = object :BroadcastReceiver(){
            override fun onReceive(context: Context, intent: Intent ){
                //Simnulando exportação de contato para Drive
                contatoCadastrado!!.exportado = "EXPORTADO"
                Contato.contatoList?.set(posicaoListaConatato, contatoCadastrado!!)
                    ConstrutorNotification.criarNotificacaoSemPendingIntent(
                            this@ListActivityContato,"CHANNELNOTIFICATION",
                            R.mipmap.ic_launcher,"Notification",
                            getString(R.string.notificacaoExportarContato),0);
                    }
        }
        return broadCastReceiver
    }

    val connectService = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {

        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
          var serviceBinder =  service as ServiceContato.BinderService
           serviceBinder.getService().startAsynckTask()
            serviceq = serviceBinder.getService()
        }

    }



}
