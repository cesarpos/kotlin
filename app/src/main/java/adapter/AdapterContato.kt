package adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import basica.contato.Contato
import com.ozeppelin.kotlincesar.R.layout.item_list
import kotlinx.android.synthetic.main.item_list.view.*

class AdapterContato(var contatoList: ArrayList<Contato>?): RecyclerView.Adapter<AdapterContato.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(item_list,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return Contato.contatoList?.size!!

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.let {
            holder.nome.text = Contato.contatoList?.get(position)?.nome
            holder.email.text = Contato.contatoList?.get(position)?.email
            holder.telefone.text = Contato.contatoList?.get(position)?.telefone
        }

    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val nome = itemView.txtItemNome
        val email = itemView.txtItemEmail
        val telefone = itemView.txtItemTelefone
    }
}