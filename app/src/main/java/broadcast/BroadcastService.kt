package broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import service.MyIntentService

class BroadcastService : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        var intentService = Intent(context,MyIntentService::class.java)
        context.startService(intentService);
    }
}
