package service

import android.app.Service
import android.content.Intent
import android.os.AsyncTask
import android.os.Binder
import android.os.IBinder

class ServiceContato : Service() {
    val asynckTaskExportarContato = AsynckTaskExportarContato()
    val instance = BinderService()

    companion object {
        var count: Int = 0
    }


    override fun onBind(intent: Intent): IBinder {
        return instance
    }

    inner class BinderService : Binder() {

        fun getService(): ServiceContato {
            return this@ServiceContato
        }

    }

    fun startAsynckTask(){
        val asyncTask =  AsynckTaskExportarContato()
        asyncTask.execute()
    }

    fun stopAsynckTask(){

    }

    fun getValorCount(): Int{
        return count
    }


     inner class AsynckTaskExportarContato: AsyncTask<Void, Void, Int>(){
        override fun doInBackground(vararg params: Void?): Int {
            while (true){
                count++
            }

            return count
        }

     }



}
