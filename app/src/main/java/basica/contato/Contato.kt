package basica.contato

import android.os.Parcel
import android.os.Parcelable


data class Contato (var nome: String?, var email: String?, var telefone: String?, var id: Int,var exportado: String?) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()) {
    }



    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nome)
        parcel.writeString(email)
        parcel.writeString(telefone)
        parcel.writeString(exportado)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Contato> {
        override fun createFromParcel(parcel: Parcel): Contato {
            return Contato(parcel)
        }

        override fun newArray(size: Int): Array<Contato?> {
            return arrayOfNulls(size)
        }

        var contatoList: ArrayList<Contato>?   = ArrayList()
    }


}